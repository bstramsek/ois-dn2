# 2. domača naloga pri predmetu OIS 2014-2015 #

Namen 2. domače naloge je demonstracija primera **spletne klepetalnice** z naslednjim ciljem:

* pregled različnih Node.js komponent,
* primer polno delujoče aplikacije s pomočjo tehnologije Node,
* asinhrona interakcija med odjemalcem in strežnikom.

## Funkcionalnosti##

* vnos sporočil v klepetalnico, ki se nato posredujejo vsem prijavljenim uporabnikom.
* sprememba vzdevka uporabnika,
* sprememba kanala na klepetalnici.

## Tehnične podrobnosti##

* strežba **statičnih datotek** (npr. HTML, CSS in JavaScript na strani odjemalca),
* obvladovanje **asinhronega pošiljanja sporočil** med strežnikom in odjemalci (WebSocket).


## Spremembe##
### Naloga 2.1 ###

Dodajte funkcionalnost prikaza grafičnih simbolov smeškov med uporabo spletne klepetalnice, 
tako da uporabnik vnese določeno zaporedje znakov, ki se ob prikazu sporočila na kanalu zamenja z vnaprej opredeljenim grafičnim simbolom.
Primer: Uporabnik na kanal posreduje sporočilo "Zanimivo ;)", ki se ob prikazu na seznamu sporočil na kanalu izpiše kot "Zanimivo wink.png".
Za prikazovanje slik uporabite kar direktne povezave na slike iz spodnje tabele in podprite naslednje preslikave:

 Smiley | Slika | Spletna povezava do slike
 --- | --- | ---
;) | wink.png | https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png
:) | smiley.png | https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png
(y) | like.png | https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png
:* | kiss.png | https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png
:( | sad.png | https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png


### Naloga 2.2 - Prikaz vzdevka prijavljenega uporabnika ###

V osnovni verziji spletne klepetalnice se nad oknom s sporočili na kanalu nahaja podatek o aktivnem kanalu, 
npr. "Skedenj". Spremenite način delovanja, da bo omenjen tekst vključeval še podatek o aktualnem vzdevku uporabnika. 
Izpis naj bo naslednje oblike: "Dejan @ Skedenj", kjer je "Dejan" vzdevek uporabnika in "Skedenj" aktualen kanal.


### Naloga 2.3 - Filter vulgarnih besed ###

Implementirajte filter angleških vulgarnih besed, kjer si pomagajte s podatki na spletni strani http://www.bannedwordlist.com/. 
Uporabniku je dovoljen vnos vulgarne beseda, filer pa se naj aktivira ob prikazu sporočila na kanalu. 
Ko pride do pojavitve besede iz omenjenega seznama, zamenjajte vsako  črko vulgarne besed z *. Filtriranje naj deluje zgolj na celotnih besedah. 
Če se vulgarna beseda pojavi kot podniz določene besede, potem filtra ne aktivirajte.

Funkcionalnost naj temelji na datoteki swearWords.txt ali swearWord.xml ali swearWords.csv, 
ki jih najdete na prej omenjeni strani in jo shranite v svoj projekt v mapo public/swearWords.txt oz. public/swearWords.xml oz. public/swearWords.csv. 
Do datoteke swearWords v svoji kodi, kjer boste implementirali filter, dostopajte preko predpomnilnika, 
kot je to implementirano v okviru strežbe statičnih spletnih strani, saj je to performančno bolj učinkovito.

Primer: Uporabnik vnese sporočilo "Ti si pa pravi fag.", sistem pa izpiše "Ti si pa pravi ***.".


### Naloga 2.4 - Stilska preobrazba ###

Trenutni izgled spletne klepetalnice je prilagojen na širino 800 točk. 
To spremenite tako, da se bo le ta prilagajal širini okna brskalnika in sicer naj področje za sporočila 
predstavlja 85% širine okna in seznam kanalov 15% širine okna. Spremenite tudi barvno lestvico 
in sicer barvo ozadja sporočil na svetlejši odtenek rumene (#FFFFCC) in barvo ozadja, 
kjer se nahaja naziv kanala na nekoliko temnejši odtenek rumene (#FFFF99).


### Naloga 2.5 - Seznam uporabnikov na kanalu ###

Na vmesnik spletne klepetalnice dodajte seznam trenutno prijavljenih uporabnikov kanala
(če ste na kanalu prijavljeni sami, potem tudi vaš), ki se mora dinamično osveževati, 
če se uporabnik pridruži kanalu ali ga zapusti. Stilsko lahko uporabiti isti slog, 
kot je uporabljen za seznam kanalov. Za implementacijo uporabite tehnologijo WebSocket.


### Naloga 2.6 - Pošiljanje zasebnih sporočil ###

Implementirajte pošiljanje zasebnih sporočil z naslednjim ukazom:

/zasebno <vzdevek> <sporočilo>
(npr. /zasebno "Dejan" "A poznaš tega novega na kanalu?"),

kjer se <sporočilo> posreduje zgolj uporabniku z vzdevkom <vzdevek>. 
Sistem mora pred pošiljanjem preveriti ali vzdevek obstaja. Če obstaja, mu posreduje sporočilo, 
sicer pa pošiljatelju posreduje sporočilo o napaki 

"Sporočila <sporočilo> uporabniku z vzdevkom <vzdevek> ni bilo mogoče posredovati.". 

Oba parametra <vzdevek> in <sporočilo> morata biti obdana z dvojnimi narekovaji ", 
kot prikazuje tudi zgornji primer.

Pošiljanje zasebnih sporočil naj bo omogočeno vsem uporabnikom spletne klepetalnice, 
ne samo uporabnikom trenutnega kanala. 
Pošiljanje zasebnega sporočila samemu sebi naj ne bo omogočeno, sistem naj vrne isto napako, 
kot v primeru, da uporabnik ne obstaja in sicer:

"Sporočilo <sporočilo> uporabniku z vzdevkom <vzdevek> ni bilo mogoče posredovati.".

Funkcionalnost implementirajte s pomočjo WebSocket tehnologije. 

Opis ukaza vključite tudi na začetno stran klepetalnice (index.html), 
kjer sta trenutno na voljo ukaza vzdevek in pridruzitev.


### Naloga 2.7 - Kreiranje kanalov, zaščitenih z geslom ###

V osnovni verziji je v sistemu implementiran ukaz

/pridruzitev <kanal>

s katerim se uporabnik pridruži v obstoječi kanal, če pa še ne obstaja, ga kreira in se mu pridruži.

Vaša naloga je implementacija kreiranja kanalov, zaščitenih z geslom in sicer z naslednjim ukazom:

/pridruzitev <kanal> <geslo>
(npr. /pridruzitev "OIS" "Porto Katsiki"),

V primeru, da kanal še ne obstaja se le ta kreira in nastavi se geslo za dostop. Ko pa kanal že obstaja, 
se mora geslo ujemati, sicer uporabniku ni dovoljena pridružitev kanalu. Če je geslo napačno, 
se pošiljatelju posreduje sporočilo o napaki "Pridružitev v kanal <kanal> ni bilo uspešno, ker je geslo napačno!". 
Oba parametra <kanal> in <geslo> morata biti obdana z dvojnimi narekovaji ", kot prikazuje tudi zgornji primer.

V primeru, ko že obstaja kanal brez gesla in se eden izmed uporabnikov želi prilključiti v takšen kanal z geslom, 
mu sistem vrne napako in sicer "Izbrani kanal <kanal> je prosto dostopen in ne zahteva prijave z geslom, 
zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.".

Ob uspešni implementaciji funkcionalnosti morata v sistemu biti na voljo oba ukaza za pridružitev kanalu.

Funkcionalnost implementirajte s pomočjo WebSocket tehnologije. Opis ukaza vključite tudi na začetno stran 
klepetalnice (index.html), kjer sta trenutno na voljo ukaza vzdevek in pridruzitev.

## Vse spremembe uveljavljene v končni uveljavitvi (zadnjič)