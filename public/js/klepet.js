var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  kanal = kanal.substring(kanal.indexOf(' @ ') + 3, kanal.length);
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz, sender) {
  var sistem = ukaz.split(" \""); 
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      if (besede.length == 2) {
        besede.shift();
        var kanal = besede.join(' ');
        this.spremeniKanal(kanal);
        
      } else if (besede.length > 2) {
        if (sistem.length != 3) {
          sporocilo = 'Nepravilen ukaz.';
          break;
        }
        
        for (var i = 0; i < sistem.length; i++) {
          var konc = sistem[i].indexOf("\"");
          if (konc >= 0) {
            sistem[i] = sistem[i].substring(0, konc);
          }
        }
        
        var soba = sistem[1];
        var geslo = sistem[2];
        
        this.socket.emit('privatnaPridruzitevZahteva', {
          socket: this.socket.id,
          soba: soba,
          geslo: geslo
        });

      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      
      if (sistem.length != 3) {
        sporocilo = 'Nepravilen ukaz.';
        break;
      }
      
      for (var i = 0; i < sistem.length; i++) {
        var konc = sistem[i].indexOf("\"");
        if (konc >= 0) {
          sistem[i] = sistem[i].substring(0, konc);
        }
        //console.log(sistem[i]);
      }
      
      var user = sistem[1];
      var message = sistem[2];
      
      if (sender == user) {
        sporocilo = "Sporočilo " + message + " uporabniku z vzdevkom " + user + " ni bilo mogoče posredovati.";
        break;
      }
      
      //besede.shift();
      //var vzdevek = besede.join(' ');
      this.socket.emit('zasebnoZahteva', {
        socket: this.socket.id,
        vzdevek: user,
        sporocilo: message,
        sender: sender
      });
      //sporocilo = "Zasebno sporočilo uporabniku z vzdevkom " + user + " je bilo posredovovano."
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};

