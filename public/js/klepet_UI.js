var besede = [];

function divElementEnostavniTekst(sporocilo) {
  var rezultat = sporocilo.replace(/</g, '&lt;');
  rezultat = rezultat.replace(/>/g, '&gt;');
  
  rezultat = rezultat.replace(/;\)/g, '<img alt=";)" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">');
  rezultat = rezultat.replace(/:\)/g, '<img alt=":)" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
  rezultat = rezultat.replace(/\(y\)/g, '<img alt="(y)" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">');
  rezultat = rezultat.replace(/:\*/g, '<img alt=":*" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">');
  rezultat = rezultat.replace(/:\(/g, '<img alt=":(" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">');
  
  return ('<div style="font-weight: bold" class="trenutniUporabniki">' + rezultat + '</div>');
  //return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function smajliji(sporocilo) {
  var rezultat = sporocilo.replace(/</g, '&lt;');
  rezultat = rezultat.replace(/>/g, '&gt;');
  
  rezultat = rezultat.replace(/;\)/g, '<img alt=";)" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">');
  rezultat = rezultat.replace(/:\)/g, '<img alt=":)" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
  rezultat = rezultat.replace(/\(y\)/g, '<img alt="(y)" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">');
  rezultat = rezultat.replace(/:\*/g, '<img alt=":*" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">');
  rezultat = rezultat.replace(/:\(/g, '<img alt=":(" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">');
  
  return (rezultat);
  //return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementEnostavniTekstPrivateRoom(sporocilo) {
  var rezultat = sporocilo.replace(/</g, '&lt;');
  rezultat = rezultat.replace(/>/g, '&gt;');
  rezultat = ' ' + rezultat;
  return $('<div style="font-weight: bold" class="privatni"><img alt="<zaščiten>" src="lock.png" height="15" width="">' + rezultat + '</div>');
}

function divElementHtmlTekst(sporocilo) {
  return $('<div class="sistemSporocilo"></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket, vzdevek) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

    
  for (var i = 0; i < besede.length; i++) {
    var re = new RegExp(besede[i], 'g'), chg = '';
    for (var j = 0; j < besede[i].length; j++) {
      chg += '*';
    }
    sporocilo = sporocilo.replace(re, chg);
  }

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo, vzdevek);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
  } else {

    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();
var mojVzdevek = '';
$(document).ready(function() {

  $('#sporocila').append('<div id="smajliSupport" class="sistemSporocilo"><code>Podpora smeškom: :) ==> <img alt=":)" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">  || :( ==> <img alt=":(" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">  || ;) ==> <img alt=";)" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">  || :* ==> <img alt=":*" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">  || (y) ==> <img alt="(y)" src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"></code></div>');
  $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  var klepetApp = new Klepet(socket);
  
  //parse SW.xml --> besede
  if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else {// code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
  xmlhttp.open("GET","swearWords.xml",false);
  xmlhttp.send();
  xmlDoc = xmlhttp.responseXML;


  for (var i = 0; i < xmlDoc.getElementsByTagName("word").length; i++) { 

    besede.push(xmlDoc.getElementsByTagName("word")[i].childNodes[0].nodeValue);
  }
  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      mojVzdevek = rezultat.vzdevek;
      sporocilo = 'Prijavljen si kot ' + mojVzdevek + '.';
      $('#kanal').text(mojVzdevek + " @ " + rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  socket.on('privatnaPridruzitevOdgovor', function(rezultat) {
    if (rezultat.koda == 1) { // public soba že obstaja
      $('#sporocila').append('<div style="font-weight: bold">Izbrani kanal ' + rezultat.wantedRoom + ' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.</div>');
    } else if (rezultat.koda == 2) {
      $('#sporocila').append('<div style="font-weight: bold">Pridružitev v kanal ' + rezultat.wantedRoom + ' ni bilo uspešno, ker je geslo napačno!</div>');
    }
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  socket.on('active-users', function (data) {
    $('#seznam-uporabnikov').empty();
    for (var i = 0; i < data.uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(data.uporabniki[i]));
    }
    $('#seznam-uporabnikov div').click(function() {
      var tx = '/zasebno "' + $(this).text() + '" "sporocilo"';
      $('#poslji-sporocilo').val(tx);
      setInputSelection(document.getElementById("poslji-sporocilo"), tx.indexOf("sporocilo"), tx.length - 1 );
    });
  });
  
  socket.on('zasebnoOdgovor', function (sporocilo) {
    var novElement = '', con = sporocilo.sporocilo.replace(/</g, '&lt;');
    con = con.replace(/>/g, '&gt;');
    if (sporocilo.uspesno) {
      if (mojVzdevek == sporocilo.sender) {
        novElement = '<div style="font-weight: italic" class="private204">(zasebno za ' + sporocilo.user + '): <i>' + smajliji(con) + '</i></div>';
      } else {
        novElement = '<div style="font-weight: bold" class="private204">' + sporocilo.sender + ' (zasebno): <i>' + smajliji(con) + '</i></div>';
      }
    } else {
      novElement = $('<div style="font-weight: italic" class="ERR404">Sporočilo ' + smajliji(con) + ' uporabniku z vzdevkom ' + sporocilo.user + ' ni bilo mogoče posredovati.</div>');
    }
    $('#sporocila').append(novElement);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });

  socket.on('kanali', function(data) {
    $('#seznam-kanalov').empty();
   
    
    for (var i = 0; i < data.length; i++) {
      if (data[i].privat) {
        $('#seznam-kanalov').append(divElementEnostavniTekstPrivateRoom(data[i].ime));
      } else {
        $('#seznam-kanalov').append(divElementEnostavniTekst(data[i].ime));
      }
    }
    
    $('#seznam-kanalov div').click(function() {
      var clas = $(this).hasClass( "privatni" );
      var kan = $(this).text();
      kan = kan.substring(1, kan.length);
      if (clas) {
        var tx = '/pridruzitev "' + kan + '" "geslo"';
        $('#poslji-sporocilo').val(tx);
        setInputSelection(document.getElementById("poslji-sporocilo"), tx.indexOf("geslo"), tx.length - 1 );
      } else {
        klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
        $('#poslji-sporocilo').focus();
      }
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  setInterval(function() {
    socket.emit('active-users');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket, mojVzdevek);
    return false;
  });
});

function setInputSelection(input, startPos, endPos) {
        input.focus();
        if (typeof input.selectionStart != "undefined") {
            input.selectionStart = startPos;
            input.selectionEnd = endPos;
        } else if (document.selection && document.selection.createRange) {
            // IE branch
            input.select();
            var range = document.selection.createRange();
            range.collapse(true);
            range.moveEnd("character", endPos);
            range.moveStart("character", startPos);
            range.select();
        }
    }
