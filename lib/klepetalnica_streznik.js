var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var active_uporabniki = [];
var uporabniki = {};
var dataKanali = [];

exports.listen = function(streznik) {

  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, {ime: 'Skedenj', prot: false} );
    uporabnikiAktivno(socket);
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajZahtevoZaZasebnoSporocilo(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajPrivatnaPridruzitevKanalu(socket);
    updateKanali(socket);
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  
  for (var name in uporabniki) {
    if (uporabniki[name].socket == socket.id) {
      delete uporabniki[name];
      break;
    }
  }
  //console.log(vzdevek);
  //console.log(socket.id);
  uporabniki[vzdevek] = {
    "socket": socket.id
  };
  
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  var obstaja = false;
  
  for (var i = 0; i < dataKanali.length; i++) {
    if (typeof dataKanali[i] !== "undefined") {
      if (dataKanali[i].ime == kanal.ime) {
        obstaja = true;
        break;
      }
    }
  }
  
  if (!obstaja) {
    var obj;
    if (kanal.prot) {
      obj = {
        ime: kanal.ime,
        privat: true,
        geslo: kanal.geslo
      };
    } else {
      obj = {
        ime: kanal.ime,
        privat: false
      };
    }
    
    dataKanali.push(obj);
  }
  
  
  socket.join(kanal.ime);
  trenutniKanal[socket.id] = kanal.ime;
  socket.emit('pridruzitevOdgovor', {kanal: kanal.ime, vzdevek: vzdevkiGledeNaSocket[socket.id]});
  socket.broadcast.to(kanal.ime).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal.ime + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal.ime);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal.ime + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
    
  }
  uporabnikiAktivno(socket);
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        
        for (var name in uporabniki) {
  		    if (uporabniki[name].socket == socket.id) {
  			    delete uporabniki[name];
  			    break;
  		    }
  	    }
  	    //console.log(vzdevek);
  	    //console.log(socket.id);
        uporabniki[vzdevek] = {
          "socket": socket.id
        };
        
        
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id]
        });
        
        //console.log(vzdevek);
        //console.log(trenutniKanal[socket.id]);
        
        uporabnikiAktivno(socket);
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajZahtevoZaZasebnoSporocilo (socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('zasebnoZahteva', function(podatki) {
    var uporabnik = podatki.vzdevek, sporocilo = podatki.sporocilo, sender = podatki.sender;
    
    if (uporabnik == sender) { //posiljanje samemu sebi ni dovoljeno 
      //socket.emit('zasebnoOdgovor', "Sporočilo " + sporocilo + " uporabniku z vzdevkom " + uporabnik + " ni bilo mogoče posredovati.");
      //console.log("posiljanje samemu sebi ni dovoljeno ");
      return;
    }
    
    if (uporabljeniVzdevki.indexOf(uporabnik) > -1) { // uporabnik obstaja
      //console.log("uporabnik obstaja");
      
      
      io.sockets.socket(uporabniki[uporabnik].socket).emit('zasebnoOdgovor', { // pošiljanje zasebnega sporočila
          uspesno: true,
          sender: sender,
          user: uporabnik,
          sporocilo: sporocilo
        });
        
        socket.emit('zasebnoOdgovor', { //pošiljanje potrdila o poslanem sporočilu
          uspesno: true,
          sender: sender,
          user: uporabnik,
          sporocilo: sporocilo
        });
      
    } else { //uporabnik ne obstaja
      //console.log("uporabnik ne obstaja");
      socket.emit('zasebnoOdgovor', {
          uspesno: false,
          sender: sender,
          user: uporabnik,
          sporocilo: sporocilo
        });
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var obstaja = false, index = -1;
    
    for (var i = 0; i < dataKanali.length; i++) {
      if (typeof dataKanali[i] !== "undefined") {
        if (dataKanali[i].ime == kanal.novKanal) {
          obstaja = true;
          index = i;
          break;
        }
      }
    }
    
    if (obstaja) {
      if (dataKanali[index].privat) {
        socket.emit('privatnaPridruzitevOdgovor', {
          koda: 2,
          wantedRoom: kanal.novKanal
        });
        return;
      }
    }
    
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, {ime: kanal.novKanal, prot: false});
  });
}

function obdelajPrivatnaPridruzitevKanalu(socket) {
  socket.on('privatnaPridruzitevZahteva', function(podatki) {
    // vse preveri
    var obstaja = false, index = -1;
    
    for (var i = 0; i < dataKanali.length; i++) {
      if (typeof dataKanali[i] !== "undefined") {
        if (dataKanali[i].ime == podatki.soba) {
          obstaja = true;
          index = i;
          break;
        }
      }
    }
    
    if (obstaja) {
      if (dataKanali[index].privat) {
        if (dataKanali[index].geslo != podatki.geslo) {
          socket.emit('privatnaPridruzitevOdgovor', {
            koda: 2,
            wantedRoom: podatki.soba
          });
        } else if (dataKanali[index].geslo == podatki.geslo) {
          socket.leave(trenutniKanal[socket.id]);
          pridruzitevKanalu(socket, {
            ime: podatki.soba, 
            prot: true,
            geslo: podatki.geslo
          });
        } else {
          socket.emit('privatnaPridruzitevOdgovor', {
            koda: 2,
            wantedRoom: podatki.soba
          });
        }
      } else { // public soba že obstaja
        socket.emit('privatnaPridruzitevOdgovor', {
          koda: 1,
          wantedRoom: podatki.soba
        });
      }
      
      
    } else {
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, {
        ime: podatki.soba, 
        prot: true,
        geslo: podatki.geslo
      });
    }
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    socket.emit('active-users');
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
    for (var name in uporabniki) {
  		if (uporabniki[name].socket == socket.id) {
  			delete uporabniki[name];
  			break;
  		}
  	}
  });
}

function uporabnikiAktivno(socket) {
  var kanal = trenutniKanal[socket.id];
  socket.on('active-users', function() {
    active_uporabniki = [];
    
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    if (uporabnikiNaKanalu.length > 0) {
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        active_uporabniki.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
      }
      
      socket.broadcast.to(kanal).emit('active-users', {
        uporabniki: active_uporabniki
      });
      socket.emit('active-users', {
        uporabniki: active_uporabniki
      });
    }
  });
}


function updateKanali(socket) {
  socket.on('kanali', function() {
    //socket.emit('kanali', io.sockets.manager.rooms);
    var temp = pomoznaFunkcija();
    
    socket.emit('kanali', temp);
    
  });
}

function pomoznaFunkcija () {
  var odprtiKanali = io.sockets.manager.rooms;
    
    for (var i = 0; i < dataKanali.length; i++) {
      var obstaja = false;
      for(var kanal in odprtiKanali) {
        kanal = kanal.substring(1, kanal.length);
        if (kanal != '') {
          if (typeof dataKanali[i] !== "undefined") {
            if (dataKanali[i].ime == kanal) {
              obstaja = true;
            }
          }
        }
      }
      if (!obstaja) {
        delete dataKanali[i];
      }
    }
    
    var temp = [];
    
    for (var i = 0; i < dataKanali.length; i++) {
      if (typeof dataKanali[i] !== "undefined") {
        temp.push({
          ime: dataKanali[i].ime,
          privat: dataKanali[i].privat
        });
      }
    }
    return temp;
}